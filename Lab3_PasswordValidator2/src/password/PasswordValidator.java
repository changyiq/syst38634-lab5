package password;

public class PasswordValidator {

 private static int MIN_LENGTH = 8;
 private static int MIN_NUM_DIGITS = 2;

 /**
  * 
  * validate length of password, blank spaces are not considered valid chars
  * 
  * @param password
  * @return
  * 
  */
 public static boolean isValidLength(String password) {
  // int length = password.length();
  if (password != null) {
   return password.trim().length() >= MIN_LENGTH;
  }
  return false;

 }

 public static boolean hasEnoughDigits(String password) {
  int count = 0;
  if (password != null) {
   for (char c : password.toCharArray()) {
    if (Character.isDigit(c)) {
     count++;
    }
   }
   return count >= MIN_NUM_DIGITS;
  }
  return false;
 }

}