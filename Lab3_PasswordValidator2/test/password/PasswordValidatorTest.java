package password;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {
 
 @Test
 public void testIsValidLength() {
  assertTrue("Invalid password length",PasswordValidator.isValidLength("1234567890"));
 }
 
 @Test
 public void testIsValidLengthException() {
  assertFalse("Invalid password length",PasswordValidator.isValidLength(null));
 }
 
 @Test
 public void testIsValidLengthExceptionSpaces() {
  assertFalse("Invalid password length",PasswordValidator.isValidLength("          "));
 }
 
 @Test
 public void testIsValidLengthBoundaryIn() {
  assertTrue("Invalid password length",PasswordValidator.isValidLength("12345678"));
 }
 
 @Test
 public void testIsValidLengthBoundaryOut() {
  assertFalse("Invalid password length",PasswordValidator.isValidLength("1234567"));
 }
 
 @Test
 public void testHasEnoughDigits() {
  assertTrue("Invalid password characters", PasswordValidator.hasEnoughDigits("12dddddd"));
 }

 @Test
 public void testHasEnoughDigitsException() {
  assertFalse("Invalid password characters", PasswordValidator.hasEnoughDigits(null));
 }
 
 @Test
 public void testHasEnoughDigitsBoundaryIn() {
  assertTrue("Invalid password characters", PasswordValidator.hasEnoughDigits("12aaaaaa"));
 }
 
 @Test
 public void testHasEnoughDigitsBoundaryOut() {
  assertFalse("Invalid password characters", PasswordValidator.hasEnoughDigits("1aaaaaaa"));
 }
 
}
